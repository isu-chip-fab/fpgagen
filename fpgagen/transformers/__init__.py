from .get_caravel_sources import get_caravel_sources
from .configure_board import configure_board
from .transform_nettype import transform_nettype
from .transform_primitives import transform_primitives
from .transform_pullup import transform_pullup
from .transform_mprj_in_tristate import transform_mprj_in_tristate
from .remove_corner_pads import remove_corner_pads
from .remove_empty_macros import remove_empty_macros
from .transform_sram_ro_port import transform_sram_ro_port
from .transform_flash_pins import transform_flash_pins
from .transform_pll import transform_pll
from .transform_por import transform_por
from .transform_project_id import transform_project_id
from .add_fpga_defines import add_fpga_defines

transformers = [
  get_caravel_sources,
  configure_board,

  transform_nettype,
  transform_primitives,
  transform_pullup,
  transform_mprj_in_tristate,
  remove_corner_pads,
  remove_empty_macros,
  transform_sram_ro_port,
  transform_flash_pins,

  transform_por,
  transform_pll,

  transform_project_id,

  add_fpga_defines,
]