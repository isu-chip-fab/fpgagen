from ..generator.transformer import PREFIX, CreateTransformer, TransformingFile

"""
These appear to the required defines to get Caravel to 
synthesize on an FPGA. 

SIM allows most sky130 cells to use behavioral models,
and USE_POWER_PINS was chosen due to pin mapping errors
although all power pins are hard-coded in the wrapper.
"""

@CreateTransformer(PREFIX)
def add_fpga_defines(env):
  return TransformingFile(env, 'fpga/defines.v', contents="""\
`define SIM
`define USE_POWER_PINS
""")
