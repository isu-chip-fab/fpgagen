import os

from ..generator.env import Env
from ..generator.transformer import Transformer, TransformingFile

"""
The PLL module is ASIC magic. Replace it with a behavioral replacement
that does nothing. The PLL is not usable in the FPGA, use the normal clock input.
"""

@Transformer('caravel/verilog/rtl/digital_pll.v')
def transform_pll(env: Env, file: TransformingFile):
  return TransformingFile(env, 'fpga/pll.v', os.path.join(env['BUILTIN_DIR'], 'fpga/pll.v'))