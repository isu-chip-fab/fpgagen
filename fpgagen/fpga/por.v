`default_nettype wire
module simple_por(
`ifdef USE_POWER_PINS
    inout vdd3v3,
    inout vdd1v8,
    inout vss3v3,
    inout vss1v8,
`endif
    output porb_h,
    output porb_l,
    output por_l
);

//    assign porb_h = 1; -- Driven via fpga_wrapper
    assign porb_l = porb_h;
    assign por_l = ~porb_l;

endmodule
