`default_nettype wire
module sky130_fd_sc_hd__udp_pwrgood_pp$P(
  UDP_OUT,
  UDP_IN ,
  VPWR
);
  output UDP_OUT;
  input  UDP_IN ;
  input  VPWR   ;

  assign UDP_OUT = UDP_IN;
endmodule

module sky130_fd_sc_hd__udp_pwrgood_pp$PG(
  UDP_OUT,
  UDP_IN ,
  VPWR,
  VGND
);
  output UDP_OUT;
  input  UDP_IN ;
  input  VPWR   ;
  input  VGND   ;

  assign UDP_OUT = UDP_IN;
endmodule

module sky130_fd_sc_hd__udp_pwrgood_pp$G(
  UDP_OUT,
  UDP_IN ,
  VGND
);
  output UDP_OUT;
  input  UDP_IN ;
  input  VGND   ;

  assign UDP_OUT = UDP_IN;
endmodule

module sky130_fd_sc_hd__udp_mux_4to2 (
    X ,
    A0,
    A1,
    A2,
    A3,
    S0,
    S1
);

    output X ;
    input  A0;
    input  A1;
    input  A2;
    input  A3;
    input  S0;
    input  S1;

    assign X = (S0 == 0 && S1 == 0) ? A0
             : (S0 == 1 && S1 == 0) ? A1
             : (S0 == 0 && S1 == 1) ? A2
             : A3;
endmodule

module sky130_fd_sc_hd__udp_mux_2to1_N (
    Y ,
    A0,
    A1,
    S
);

    output Y ;
    input  A0;
    input  A1;
    input  S ;

    assign Y = ~(S == 0 ? A0 : A1);
endmodule

module sky130_fd_sc_hd__udp_mux_2to1 (
    X ,
    A0,
    A1,
    S
);

    output X ;
    input  A0;
    input  A1;
    input  S ;

    assign X = S == 0 ? A0 : A1;
endmodule

module sky130_fd_sc_hd__udp_dlatch$PR_pp$PG$N (
    Q       ,
    D       ,
    GATE    ,
    RESET   ,
    NOTIFIER,
    VPWR    ,
    VGND
);

    output Q       ;
    input  D       ;
    input  GATE    ;
    input  RESET   ;
    input  NOTIFIER;
    input  VPWR    ;
    input  VGND    ;

    reg Q;

    always @(D or GATE or RESET)
    begin
      if (RESET)
        Q <= 0;
      else
        if (GATE)
          Q <= D;
        else
          Q <= Q;
    end 
endmodule

module sky130_fd_sc_hd__udp_dlatch$PR (
    Q       ,
    D       ,
    GATE    ,
    RESET
);

    output Q       ;
    input  D       ;
    input  GATE    ;
    input  RESET   ;

    reg Q;

    always @(D or GATE or RESET)
    begin
      if (RESET)
        Q <= 0;
      else
        if (GATE)
          Q <= D;
        else
          Q <= Q;
    end 
endmodule

module sky130_fd_sc_hd__udp_dlatch$P_pp$PG$N (
    Q       ,
    D       ,
    GATE    ,
    NOTIFIER,
    VPWR    ,
    VGND
);

    output Q       ;
    input  D       ;
    input  GATE    ;
    input  NOTIFIER;
    input  VPWR    ;
    input  VGND    ;

    reg Q;

    always @(D or GATE)
    begin
      if (GATE)
        Q <= D;
      else
        Q <= Q;
    end 
endmodule

module sky130_fd_sc_hd__udp_dlatch$P (
    Q       ,
    D       ,
    GATE    ,
    RESET
);

    output Q       ;
    input  D       ;
    input  GATE    ;
    input  RESET   ;

    reg Q;

    always @(D or GATE)
    begin
      if (GATE)
        Q <= D;
      else
        Q <= Q;
    end 
endmodule

module sky130_fd_sc_hd__udp_dlatch$lP_pp$PG$N (
    Q       ,
    D       ,
    GATE    ,
    NOTIFIER,
    VPWR    ,
    VGND
);

    output Q       ;
    input  D       ;
    input  GATE    ;
    input  NOTIFIER;
    input  VPWR    ;
    input  VGND    ;

    reg Q;

    always @(D or GATE)
    begin
      if (GATE)
        Q <= D;
      else
        Q <= Q;
    end 
endmodule

module sky130_fd_sc_hd__udp_dlatch$lP (
    Q       ,
    D       ,
    GATE    ,
    RESET
);

    output Q       ;
    input  D       ;
    input  GATE    ;
    input  RESET   ;

    reg Q;

    always @(D or GATE)
    begin
      if (GATE)
        Q <= D;
      else
        Q <= Q;
    end 
endmodule

module sky130_fd_sc_hd__udp_dff$PS_pp$PG$N (
    Q       ,
    D       ,
    CLK     ,
    SET     ,
    NOTIFIER,
    VPWR    ,
    VGND
);

    output Q       ;
    input  D       ;
    input  CLK     ;
    input  SET     ;
    input  NOTIFIER;
    input  VPWR    ;
    input  VGND    ;

    reg Q;

    always @(posedge CLK or posedge SET)
      if (SET)
        Q <= 1;
      else
        Q <= D;
endmodule

module sky130_fd_sc_hd__udp_dff$PS (
    Q       ,
    D       ,
    CLK     ,
    SET
);

    output Q       ;
    input  D       ;
    input  CLK     ;
    input  SET     ;

    reg Q;

    always @(posedge CLK or posedge SET)
      if (SET)
        Q <= 1;
      else
        Q <= D;
endmodule

module sky130_fd_sc_hd__udp_dff$PR_pp$PG$N (
    Q       ,
    D       ,
    CLK     ,
    RESET     ,
    NOTIFIER,
    VPWR    ,
    VGND
);

    output Q       ;
    input  D       ;
    input  CLK     ;
    input  RESET     ;
    input  NOTIFIER;
    input  VPWR    ;
    input  VGND    ;

    reg Q;

    always @(posedge CLK or posedge RESET)
      if (RESET)
        Q <= 0;
      else
        Q <= D;
endmodule

module sky130_fd_sc_hd__udp_dff$PR (
    Q       ,
    D       ,
    CLK     ,
    RESET
);

    output Q       ;
    input  D       ;
    input  CLK     ;
    input  RESET     ;

    reg Q;

    always @(posedge CLK or posedge RESET)
      if (RESET)
        Q <= 0;
      else
        Q <= D;
endmodule

module sky130_fd_sc_hd__udp_dff$P_pp$PG$N (
    Q       ,
    D       ,
    CLK     ,
    NOTIFIER,
    VPWR    ,
    VGND
);

    output Q       ;
    input  D       ;
    input  CLK     ;
    input  NOTIFIER;
    input  VPWR    ;
    input  VGND    ;

    reg Q;

    always @(posedge CLK)
      Q <= D;
endmodule

module sky130_fd_sc_hd__udp_dff$P (
    Q       ,
    D       ,
    CLK
);

    output Q       ;
    input  D       ;
    input  CLK     ;

    reg Q;

    always @(posedge CLK)
      Q <= D;
endmodule

module sky130_fd_sc_hd__udp_dff$NSR_pp$PG$N (
    Q       ,
    SET     ,
    RESET   ,
    CLK_N   ,
    D       ,
    NOTIFIER,
    VPWR    ,
    VGND
);

    output Q       ;
    input  SET     ;
    input  RESET   ;
    input  CLK_N   ;
    input  D       ;
    input  NOTIFIER;
    input  VPWR    ;
    input  VGND    ;

    reg Q;

    always @(negedge CLK_N or posedge SET or posedge RESET)
      if (SET)
        Q <= 1;
      else if (RESET)
        Q <= 0;
      else
        Q <= D;
endmodule

module sky130_fd_sc_hd__udp_dff$NSR (
    Q       ,
    SET     ,
    RESET   ,
    CLK_N   ,
    D
);

    output Q       ;
    input  SET     ;
    input  RESET   ;
    input  CLK_N   ;
    input  D       ;

    reg Q;

    always @(negedge CLK_N or posedge SET or posedge RESET)
      if (SET)
        Q <= 1;
      else if (RESET)
        Q <= 0;
      else
        Q <= D;
endmodule

module sky130_ef_sc_hd__fill_12 ();
endmodule

module sky130_ef_sc_hd__fill_8 ();
endmodule

module sky130_ef_sc_hd__fakediode_2 (DIODE);
  input DIODE;
endmodule

module sky130_ef_sc_hd__decap_12 ();
endmodule




module sky130_fd_sc_hvl__udp_pwrgood_pp$P(
  UDP_OUT,
  UDP_IN ,
  VPWR
);
  output UDP_OUT;
  input  UDP_IN ;
  input  VPWR   ;

  assign UDP_OUT = (VPWR == 'b1 && UDP_IN == 'b1) ? 'b1
                  : (VPWR == 'b1 && UDP_IN == 'b0) ? 'b0
                  : 'bx;
endmodule

module sky130_fd_sc_hvl__udp_pwrgood_pp$PG(
  UDP_OUT,
  UDP_IN ,
  VPWR,
  VGND
);
  output UDP_OUT;
  input  UDP_IN ;
  input  VPWR   ;
  input  VGND   ;

  assign UDP_OUT = (VPWR == 'b1 && VGND == 'b0 && UDP_IN == 'b1) ? 'b1
                  : (VPWR == 'b1 && VGND == 'b0 && UDP_IN == 'b0) ? 'b0
                  : 'bx;
endmodule

module sky130_fd_sc_hvl__udp_pwrgood_pp$G(
  UDP_OUT,
  UDP_IN ,
  VGND
);
  output UDP_OUT;
  input  UDP_IN ;
  input  VGND   ;

  assign UDP_OUT = (VGND == 'b0 && UDP_IN == 'b1) ? 'b1
                  : (VGND == 'b0 && UDP_IN == 'b0) ? 'b0
                  : 'bx;
endmodule
`default_nettype none