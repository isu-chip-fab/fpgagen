import traceback
from typing import List, Callable
from .util import readfile, writefile
from .env import Env
import re
import os

class TransformError(Exception):
  pass

class TransformingFile:
  """
  A file in the process of being transformed.
  The file is read from src_path and written to dest_dir/path
  
  Transformers will match against `path`, so `dest_dir` should 
    be the root of the destination source hierarchy.

  If logging is enabled, each transform step is written to destdir/path.t*

  If the src_path == None, then the contents of the file
    should be provided in the contents argument.
  """
  def __init__(self, env, path: str, src_path: str=None, dest_dir: str=None, contents: str=None):
    if dest_dir == None:
      dest_dir = env['SOURCE_OUT_DIR']
    self.path = path
    self.dest_dir = dest_dir
    if src_path == None and contents == None:
      raise FileNotFoundError(f'TransformingFile has no src_path and no contents {path}')

    if src_path != None:
      contents = readfile(src_path)
    
    self.contents = contents 

  @property
  def abspath(self):
    return os.path.join(self.dest_dir, self.path)
  
  def writelog(self, version: str):
    if hasattr(self, 'old_contents'):
      if self.old_contents == self.contents: return
    self.old_contents = self.contents
    writefile(self.contents, self.dest_dir, self.path + version)

  def write(self):
    # Skip overwriting unchanged files so Vivado doesn't think
    # they are changed when they are not
    if os.path.exists(self.abspath):
      if self.contents == readfile(self.abspath):
        return
    writefile(self.contents, self.dest_dir, self.path)

  def __str__(self):
    return self.path
  
  def __repr__(self):
    return self.path



class _Transformer:
  """
  A transform function to act on a set of files.
  Callback should accept a single TransformingFile and Env and modify it in-place.
  It will be called once for each path matching paths:

  transform
   - Callback which takes a TransformingFile and Env as an argument and modifies it in-place.
   - If it returns False, then the file is removed completely

  paths
   - String for a single path to match against, starting from user project root
   - re.Pattern object from re.compile to match against
   - Array of either of the above
   - Default of None = all files
  """
  def __init__(self, transform: 'Callable[[TransformingFile], False|TransformingFile|None]', paths: 'list[re.Pattern|str]|re.Pattern|str|None'=None):
    self.transform = transform
    self.paths = None if paths == None else (paths if isinstance(paths, list) else [paths])

  def name(self):
    return self.transform.__name__

  def transform_extension(self, logIndex):
    return f'.debug{logIndex}-{self.name()}'

  def should_transform(self, file: TransformingFile):
    if self.paths == None:
      return True
    for path in self.paths:
        if not isinstance(path, str):
          if path.match(file.path):
            return True
        else:
          if path == file.path:
            return True
    return False
  
  def transform_if_needed(self, env: Env, file: TransformingFile, logIndex=None):
    if self.should_transform(file):
      ret = self.transform(env, file)
      if ret == False:
        return None
      elif isinstance(ret, TransformingFile):
        if logIndex != None: ret.writelog(self.transform_extension(logIndex))
        return ret
      else:
        if logIndex != None: file.writelog(self.transform_extension(logIndex))
        return file
    return file

  def __call__(self, env: Env, files, logIndex=None):
    transformed = (self.transform_if_needed(env, f, logIndex) for f in files)
    return [f for f in transformed if f != None]

def Transformer(paths=None):
  """
  A Transformer can return:
   1) False to delete the file,
   2) a replacement TransformingFile instance, in which case the original is removed, or
   3) None to modify in-place.
  """
  return lambda callback, paths=paths: _Transformer(callback, paths)

def GlobalTransformer(callback):
  """
  A GlobalTransformer can return:
   1) False to delete the file,
   2) a replacement TransformingFile instance, in which case the original is removed, or
   3) None to modify in-place.
  """
  return _Transformer(callback)



PREFIX = 0
POSTFIX = 1

class _CreateTransformer(_Transformer):
  def __init__(self, create, fix):
    self.transform = create
    self.fix = fix

  def __call__(self, env: Env, files, logIndex=None):
    additions = self.transform(env)
    
    # Make a singleton into a list
    if isinstance(additions, TransformingFile):
      additions = [additions]
    
    # Make additions is a list and not another kind of iterable
    if not isinstance(additions, list):
      additions = list(additions)

    # Check that every item is a TransformingFile
    for a in additions:
      if not isinstance(a, TransformingFile):
        raise RuntimeError('CreateTransformer returned something that is not a TransformingFile')
    
    # Save the intermediate version if needed
    if logIndex != None:
      for f in additions:
        f.writelog(self.transform_extension(logIndex))

    if self.fix == PREFIX:
      additions.extend(files)
      files = additions
    else:
      files.extend(additions)
    return files

def CreateTransformer(fix):
  """
  A CreateTransformer must return:
  1) a TransformingFile instance, or
  2) an iterable of TransformingFile instances
  """
  return lambda callback, fix=fix: _CreateTransformer(callback, fix)



class TransformerGroup(_Transformer):
  def __init__(self, *transformers: _Transformer):
    self.transformers = transformers
    self.current_transformer = None
  
  @property
  def transform(self):
    return None if self.current_transformer == None else self.current_transformer.transform

  def __call__(self, env: Env, files: List[TransformingFile], logIndex=None):
    for i, t in enumerate(self.transformers):
      self.current_transformer = t
      try:
        if not isinstance(t, _Transformer):
          raise RuntimeError(f'{t} is not a transformer', t)
        files = t(env, files, f'{logIndex}-{i:02}' if logIndex != None else None)
      except Exception as e:
        traceback.print_exc()
        if not isinstance(t, TransformerGroup):
          print(f'ERROR: While running transformer {t.name()} in')
          print('  ' + t.transform.__globals__['__file__'])
          exit(1)
    self.current_transformer = None
    return files


def run_transformers(env: Env, transformers: 'list[_Transformer]', logging=True):
  if logging == False:
    # Clean up old debug log files:
    for root, _, files in os.walk(env['SOURCE_OUT_DIR']):
      for f in files:
        if '.debug-' in f:
          os.remove(os.path.join(root, f))

  files = TransformerGroup(*transformers)(env, [], '' if logging == True else None)

  for f in files:
    f.write()

  print('Successfully transformed files')

  return [f.abspath for f in files]