import re
from fpgagen.generator.transformer import CreateTransformer, GlobalTransformer, Transformer, TransformerGroup, TransformingFile, POSTFIX
from fpgagen.transformers import transformers as default_transformers

transformers = default_transformers

## Example Transformers
# See env['BUILTIN_DIR']/fpgagen/transformers to reference the builtin transformers

# # A transformer which adds two files to the end of the sources list
# # Use PREFIX to prepend to the sources list. This sets the compile order.
# @CreateTransformer(POSTFIX)
# def add_file(env):
#   return [
#     TransformingFile(env, 'destination/path/file.v', 'file.v'), # Relative paths are from the working directory when invoking fpgagen
#     TransformingFile(env, 'destination/path/file.v', '/path/to/file.v'), # Or use an absolute path
#     TransformingFile(env, 'destination/path/file.v', os.path.join(env['PROJECT_DIR'], 'file.v')), # Or use a config variable
#     TransformingFile(env, 'destination/path/file.v', contents='hello'), # Or use the specified contents instead of reading a file
#   ]

# # Delete a file by returning False from a transformer
# @Transformer('path/to/file.v')
# def delete_file(env, file: TransformingFile):
#   return False

# # Replace a file by returning a new TransformingFile instance
# @Transformer('path/to/file.v')
# def replace_file(env, file: TransformingFile):
#   return TransformingFile('path/to/new/file.v', contents='file contents')

# # Modify a file in-place by not returning anything or returning the file
# @Transformer('path/to/change.v')
# def change_ones(env, file: TransformingFile):
#   file.contents = file.contents.replace('1', '0')

# # Transform all files (equivalent to @Transformer(re.compile(r'.*'))))
# @GlobalTransformer
# def change_all_ones(env, file: TransformingFile):
#   file.contents = file.contents.replace('1', '0')

# # Transform files matching regex or path, any combination is allowed
# @Transformer([
#   'path/to/specific.v',
#   re.compile(r'.*\.v'),
# ])
# def change_many_ones(env, file: TransformingFile):
#   file.contents = file.contents.replace('1', '0')

# # Group transforms together to perform a common purpose
# ones = TransformerGroup(
#   change_ones,
#   change_all_ones,
#   change_many_ones
# )

# # Replace the fpga_wrapper.v file
# @Transformer('fpga/arty-a7-100/wrapper.v')
# def change_many_ones(env, file: TransformingFile):
#   return TransformingFile('fpga/arty-a7-100/wrapper.v', '.')

# # The final list of transformers to use.
# # Must be a list of _Transformer instances from the above decorators
# transformers = [
#   *default_transformers,
#   add_file,
#   ones,
# ]