from ..generator.env import Env
from ..generator.transformer import Transformer, TransformingFile

"""
The Caravel wrapper has the wrong port direction set for the flash pins.
Change them from output to inout.
"""

@Transformer('caravel/verilog/rtl/caravel.v')
def transform_flash_pins(env: Env, file: TransformingFile):
  file.contents = file.contents.replace(r'output flash_', r'inout flash_')
