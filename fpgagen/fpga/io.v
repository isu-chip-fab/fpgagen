`default_nettype wire
module sky130_ef_io__vccd_hvc_pad (AMUXBUS_A, AMUXBUS_B, DRN_HVC,
	SRC_BDY_HVC, VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VCCD_PAD, VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout DRN_HVC;
  inout SRC_BDY_HVC;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VCCD_PAD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
endmodule

module sky130_ef_io__vccd_lvc_pad (AMUXBUS_A, AMUXBUS_B,
	DRN_LVC1, DRN_LVC2, SRC_BDY_LVC1, SRC_BDY_LVC2, BDY2_B2B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD, VCCD_PAD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout DRN_LVC1;
  inout DRN_LVC2;
  inout SRC_BDY_LVC1;
  inout SRC_BDY_LVC2;
  inout BDY2_B2B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VCCD_PAD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
endmodule

module sky130_ef_io__vdda_lvc_pad (AMUXBUS_A, AMUXBUS_B,
	DRN_LVC1, DRN_LVC2, SRC_BDY_LVC1, SRC_BDY_LVC2, BDY2_B2B,
	VSSA, VDDA, VDDA_PAD, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout DRN_LVC1;
  inout DRN_LVC2;
  inout SRC_BDY_LVC1;
  inout SRC_BDY_LVC2;
  inout BDY2_B2B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VDDA_PAD;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
endmodule

module sky130_ef_io__vdda_hvc_pad (AMUXBUS_A, AMUXBUS_B, DRN_HVC,
	SRC_BDY_HVC,VSSA, VDDA, VDDA_PAD, VSWITCH, VDDIO_Q, VCCHIB,
	VDDIO, VCCD, VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout DRN_HVC;
  inout SRC_BDY_HVC;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VDDA_PAD;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
endmodule

module sky130_ef_io__vddio_lvc_pad (AMUXBUS_A, AMUXBUS_B,
	DRN_LVC1, DRN_LVC2, SRC_BDY_LVC1, SRC_BDY_LVC2, BDY2_B2B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VDDIO_PAD, VCCD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout DRN_LVC1;
  inout DRN_LVC2;
  inout SRC_BDY_LVC1;
  inout SRC_BDY_LVC2;
  inout BDY2_B2B;
  inout VDDIO;
  inout VDDIO_PAD;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
  assign VDDIO_Q = VDDIO;
endmodule

module sky130_ef_io__vddio_hvc_pad (AMUXBUS_A, AMUXBUS_B, DRN_HVC,
	SRC_BDY_HVC,VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO,
	VDDIO_PAD, VCCD, VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout DRN_HVC;
  inout SRC_BDY_HVC;
  inout VDDIO;
  inout VDDIO_PAD;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
  assign VDDIO_Q = VDDIO;
endmodule

module sky130_ef_io__vssd_lvc_pad (AMUXBUS_A, AMUXBUS_B,
	DRN_LVC1, DRN_LVC2, SRC_BDY_LVC1, SRC_BDY_LVC2, BDY2_B2B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSD_PAD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout DRN_LVC1;
  inout DRN_LVC2;
  inout SRC_BDY_LVC1;
  inout SRC_BDY_LVC2;
  inout BDY2_B2B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSD_PAD;
  inout VSSIO_Q;
  inout VSSIO;
endmodule

module sky130_ef_io__vssd_hvc_pad (AMUXBUS_A, AMUXBUS_B, DRN_HVC,
	SRC_BDY_HVC, VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSD_PAD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout DRN_HVC;
  inout SRC_BDY_HVC;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSD_PAD;
  inout VSSIO_Q;
  inout VSSIO;
endmodule

module sky130_ef_io__vssio_lvc_pad (AMUXBUS_A, AMUXBUS_B,
	DRN_LVC1, DRN_LVC2, SRC_BDY_LVC1, SRC_BDY_LVC2, BDY2_B2B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSIO_PAD, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout DRN_LVC1;
  inout DRN_LVC2;
  inout SRC_BDY_LVC1;
  inout SRC_BDY_LVC2;
  inout BDY2_B2B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
  inout VSSIO_PAD;
  assign VSSIO_Q = VSSIO;
endmodule

module sky130_ef_io__vssio_hvc_pad (AMUXBUS_A, AMUXBUS_B, DRN_HVC,
	SRC_BDY_HVC,VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSIO_PAD, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout DRN_HVC;
  inout SRC_BDY_HVC;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
  inout VSSIO_PAD;
  assign VSSIO_Q = VSSIO;
endmodule

module sky130_ef_io__vssa_lvc_pad (AMUXBUS_A, AMUXBUS_B,
	DRN_LVC1, DRN_LVC2, SRC_BDY_LVC1, SRC_BDY_LVC2, BDY2_B2B,
	VSSA, VSSA_PAD, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout DRN_LVC1;
  inout DRN_LVC2;
  inout SRC_BDY_LVC1;
  inout SRC_BDY_LVC2;
  inout BDY2_B2B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSA_PAD;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
endmodule

module sky130_ef_io__vssa_hvc_pad (AMUXBUS_A, AMUXBUS_B, DRN_HVC,
	SRC_BDY_HVC,VSSA, VSSA_PAD, VDDA, VSWITCH, VDDIO_Q, VCCHIB,
	VDDIO, VCCD, VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout DRN_HVC;
  inout SRC_BDY_HVC;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSA_PAD;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
endmodule

module sky130_ef_io__corner_pad (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
  
  wire NO_BLACK_BOX;
endmodule

module sky130_fd_io__com_bus_slice (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
endmodule

module sky130_ef_io__com_bus_slice_1um (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
endmodule

module sky130_ef_io__com_bus_slice_5um (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
endmodule

module sky130_ef_io__com_bus_slice_10um (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
endmodule

module sky130_ef_io__com_bus_slice_20um (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
endmodule

module sky130_ef_io__gpiov2_pad (IN_H, PAD_A_NOESD_H, PAD_A_ESD_0_H, PAD_A_ESD_1_H,
    PAD, DM, HLD_H_N, IN, INP_DIS, IB_MODE_SEL, ENABLE_H, ENABLE_VDDA_H,
    ENABLE_INP_H, OE_N, TIE_HI_ESD, TIE_LO_ESD, SLOW, VTRIP_SEL, HLD_OVR,
    ANALOG_EN, ANALOG_SEL, ENABLE_VDDIO, ENABLE_VSWITCH_H, ANALOG_POL, OUT,
    AMUXBUS_A, AMUXBUS_B, VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
    VSSIO, VSSD, VSSIO_Q
    );
  input OUT;
  input OE_N;
  input HLD_H_N;
  input ENABLE_H;
  input ENABLE_INP_H;
  input ENABLE_VDDA_H;
  input ENABLE_VSWITCH_H;
  input ENABLE_VDDIO;
  input INP_DIS;
  input IB_MODE_SEL;
  input VTRIP_SEL;
  input SLOW;
  input HLD_OVR;
  input ANALOG_EN;
  input ANALOG_SEL;
  input ANALOG_POL;
  input [2:0] DM;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
  inout PAD;
  inout PAD_A_NOESD_H,PAD_A_ESD_0_H,PAD_A_ESD_1_H;
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  output IN;
  output IN_H;
  output TIE_HI_ESD, TIE_LO_ESD;
endmodule

module sky130_ef_io__vddio_hvc_clamped_pad (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VDDIO_PAD, VCCD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_PAD;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
  
  assign VDDIO = VDDIO_PAD;
  assign VDDIO_PAD = 1'bz;
endmodule

module sky130_ef_io__vssio_hvc_clamped_pad (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSIO_PAD, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
  inout VSSIO_PAD;
  
  assign VSSIO = VSSIO_PAD;
  assign VSSIO_PAD = 1'bz;
endmodule

module sky130_ef_io__vdda_hvc_clamped_pad (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VDDA_PAD, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VDDA_PAD;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
  
  assign VDDA = VDDA_PAD;
  assign VDDA_PAD = 1'bz;
endmodule

module sky130_ef_io__vssa_hvc_clamped_pad (AMUXBUS_A, AMUXBUS_B,
	VSSA, VSSA_PAD, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSA_PAD;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
  
  assign VSSA = VSSA_PAD;
  assign VSSA_PAD = 1'bz;
endmodule

module sky130_ef_io__vccd_lvc_clamped_pad (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD, VCCD_PAD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VCCD_PAD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
  
  assign VCCD = VCCD_PAD;
  assign VCCD_PAD = 1'bz;
endmodule

module sky130_ef_io__vssd_lvc_clamped_pad (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSD_PAD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSD_PAD;
  inout VSSIO_Q;
  inout VSSIO;
  
  assign VSSD = VSSD_PAD;
  assign VSSD_PAD = 1'bz;
endmodule

module sky130_ef_io__vccd_lvc_clamped2_pad (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD, VCCD_PAD,
	VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VCCD_PAD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
  
  assign VCCD = VCCD_PAD;
  assign VCCD_PAD = 1'bz;
endmodule

module sky130_ef_io__vssd_lvc_clamped2_pad (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSD_PAD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSD_PAD;
  inout VSSIO_Q;
  inout VSSIO;
  
  assign VSSD = VSSD_PAD;
  assign VSSD_PAD = 1'bz;
endmodule

module sky130_ef_io__vccd_lvc_clamped3_pad (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD, VCCD_PAD,
	VSSIO, VSSD, VSSIO_Q, VCCD1, VSSD1
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VCCD_PAD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
  inout VCCD1;
  inout VSSD1;
  
  assign VCCD = VCCD_PAD;
  assign VCCD_PAD = 1'bz;
endmodule

module sky130_ef_io__vssd_lvc_clamped3_pad (AMUXBUS_A, AMUXBUS_B,
	VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
	VSSIO, VSSD, VSSD_PAD, VSSIO_Q, VCCD1, VSSD1
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSD_PAD;
  inout VSSIO_Q;
  inout VSSIO;
  inout VCCD1;
  inout VSSD1;
  
  assign VSSD = VSSD_PAD;
  assign VSSD_PAD = 1'bz;
endmodule

module sky130_ef_io__top_power_hvc (AMUXBUS_A, AMUXBUS_B, DRN_HVC,
	P_CORE, P_PAD, SRC_BDY_HVC, VSSA, VDDA, VSWITCH, VDDIO_Q,
	VCCHIB, VDDIO, VCCD, VSSIO, VSSD, VSSIO_Q
);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout DRN_HVC;
  inout P_CORE;
  inout P_PAD;
  inout SRC_BDY_HVC;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
endmodule

module sky130_ef_io__analog_pad (AMUXBUS_A, AMUXBUS_B, P_PAD, P_CORE);
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout P_PAD;
  inout P_CORE;
  supply1 VCCD;
  supply1 VCCHIB;
  supply1 VDDA;
  supply1 VDDIO;
  supply1 VDDIO_Q;
  supply0 VSSA;
  supply0 VSSD;
  supply0 VSSIO;
  supply0 VSSIO_Q;
  supply1 VSWITCH;
endmodule

module sky130_ef_io__gpiov2_pad_wrapped (IN_H, PAD_A_NOESD_H, PAD_A_ESD_0_H, PAD_A_ESD_1_H,
    PAD, DM, HLD_H_N, IN, INP_DIS, IB_MODE_SEL, ENABLE_H, ENABLE_VDDA_H,
    ENABLE_INP_H, OE_N, TIE_HI_ESD, TIE_LO_ESD, SLOW, VTRIP_SEL, HLD_OVR,
    ANALOG_EN, ANALOG_SEL, ENABLE_VDDIO, ENABLE_VSWITCH_H, ANALOG_POL, OUT,
    AMUXBUS_A, AMUXBUS_B, VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD,
    VSSIO, VSSD, VSSIO_Q
    );
  input OUT;
  input OE_N;
  input HLD_H_N;
  input ENABLE_H;
  input ENABLE_INP_H;
  input ENABLE_VDDA_H;
  input ENABLE_VSWITCH_H;
  input ENABLE_VDDIO;
  input INP_DIS;
  input IB_MODE_SEL;
  input VTRIP_SEL;
  input SLOW;
  input HLD_OVR;
  input ANALOG_EN;
  input ANALOG_SEL;
  input ANALOG_POL;
  input [2:0] DM;
  inout VDDIO;
  inout VDDIO_Q;
  inout VDDA;
  inout VCCD;
  inout VSWITCH;
  inout VCCHIB;
  inout VSSA;
  inout VSSD;
  inout VSSIO_Q;
  inout VSSIO;
  inout PAD;
  inout PAD_A_NOESD_H,PAD_A_ESD_0_H,PAD_A_ESD_1_H;
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  output IN;
  output IN_H;
  output TIE_HI_ESD, TIE_LO_ESD;
  
  wire oe;
  assign oe = DM[2] & DM[1] & ~DM[0] & ~OE_N;
  
  assign TIE_HI_ESD = 1;
  assign TIE_LO_ESD = 0;
  assign IN = INP_DIS ? 0 : (oe ? OUT : PAD);
  assign IN_H = INP_DIS ? 0 : (oe ? OUT : PAD);
  assign PAD = oe ? OUT : 1'bz;
  
endmodule

module sky130_fd_io__top_xres4v2 (AMUXBUS_A, AMUXBUS_B, VSSA, VDDA, VSWITCH, VDDIO_Q, VCCHIB, VDDIO, VCCD, VSSIO, VSSD, VSSIO_Q, PAD, TIE_WEAK_HI_H, TIE_HI_ESD, TIE_LO_ESD, PAD_A_ESD_H, XRES_H_N, DISABLE_PULLUP_H, ENABLE_H, EN_VDDIO_SIG_H, INP_SEL_H, FILT_IN_H, PULLUP_H, ENABLE_VDDIO);
  output XRES_H_N;
  inout AMUXBUS_A;
  inout AMUXBUS_B;
  inout PAD;
  input DISABLE_PULLUP_H;
  input ENABLE_H;
  input EN_VDDIO_SIG_H;
  input INP_SEL_H;
  input FILT_IN_H;
  inout PULLUP_H;
  input ENABLE_VDDIO;
  input VCCD;
  input VCCHIB;
  input VDDA;
  input VDDIO;
  input VDDIO_Q;
  input VSSA;
  input VSSD;
  input VSSIO;
  input VSSIO_Q;
  input VSWITCH;
  inout PAD_A_ESD_H;
  output TIE_HI_ESD;
  output TIE_LO_ESD;
  inout TIE_WEAK_HI_H;
  
  assign XRES_H_N = PAD;
endmodule

`default_nettype none