import re

from ..generator.transformer import Transformer, TransformingFile

"""
The corner pads are unnecessary and cause several synthesis errors,
so just remove them.
"""

@Transformer('caravel/verilog/rtl/chip_io.v')
def remove_corner_pads(env, file: TransformingFile):
  file.contents = re.sub(r'(sky130_ef_io__corner_pad[^;]+;)', r'/*\1*/', file.contents)