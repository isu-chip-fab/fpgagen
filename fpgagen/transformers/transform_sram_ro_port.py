import re

from ..generator.env import Env
from ..generator.transformer import Transformer, TransformingFile

"""
SRAM RO ports are unused in this revision and cause synthesis errors
because management_core_wrapper does not implement them, but they are still
connected.
"""
@Transformer([
  'caravel/verilog/rtl/housekeeping.v',
  'caravel/verilog/rtl/caravel.v'
])
def transform_sram_ro_port(env: Env, file: TransformingFile):
    file.contents = re.sub(r'.*sram_ro.*', '', file.contents)