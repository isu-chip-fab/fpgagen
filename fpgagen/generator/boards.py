BOARDS = {
  'arty-a7-100': {
    'VIVADO_BOARD': 'digilentinc.com:arty-a7-100:part0:1.0',
    'VIVADO_PART': 'xc7a100tcsg324-1',
    'FPGA_WRAPPER': 'fpga/arty/fpga_wrapper.v',
    'FPGA_XDC': 'fpga/arty/Arty-A7-100.xdc',
  },
}