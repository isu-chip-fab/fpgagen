import re

from ..generator.env import Env
from ..generator.transformer import GlobalTransformer, TransformingFile

"""
Most of the files have default_nettype none set, but missing wire definitions.
This is apparently accepted in verilator and yosis, but Vivado gets angry.

Wrap all module and primitive definitions in default_nettype wire, 
and all functions and tasks in default_nettype none
"""

REGEX = re.compile(r'(?ms)(?:/\*.*?\*/|//.*?$|(primitive.*?endprimitive|module.*?endmodule)|(task.*?endtask|function.*?endfunction))')
@GlobalTransformer
def transform_nettype(env: Env, file: TransformingFile):
    def replace(match):
        (wire, none) = match.groups()
        if wire != None: return '`default_nettype wire\n' + wire + '\n`default_nettype none'
        if none != None: return '`default_nettype none\n' + none + '\n`default_nettype wire'
        return match[0]

    file.contents = REGEX.sub(replace, file.contents)