from configparser import ConfigParser
import os
import re

from .boards import BOARDS

class Env:
    defaults = {
        ## Caravel paths
        'MCW_ROOT': lambda e: os.path.join(e['PROJECT_DIR'], 'mgmt_core_wrapper'),
        'CARAVEL_ROOT': lambda e: os.path.join(e['PROJECT_DIR'], 'caravel'),
        'USER_PROJECT_VERILOG': lambda e: os.path.join(e['PROJECT_DIR'], 'verilog'),
        'VERILOG_PATH': lambda e: os.path.join(e['MCW_ROOT'], 'verilog'),
        'CARAVEL_PATH': lambda e: os.path.join(e['CARAVEL_ROOT'], 'verilog'),
        'PDK_ROOT': lambda e: os.path.join(e['PROJECT_DIR'], 'dependencies', 'pdks'),
        'PDK': 'sky130A',
        
        # Output directory to save all files
        'OUT_DIR': lambda e: os.path.normpath(os.path.join(e['PROJECT_DIR'], os.pardir, e['PROJECT_NAME'])),
        'SOURCE_OUT_DIR': lambda e: os.path.join(e['OUT_DIR'], 'sources'),

        # Directory to find builtin verilog files
        'BUILTIN_DIR': os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)), 

        # Path to the python file which exports an array `transformers` of transformers to use
        'TRANSFORMERS': '(default, see `fpgagen template`)',
        
        # Board configuration (see generator/boards.py)
        'BOARD': 'arty-a7-100',

        # Project directory and name
        'PROJECT_DIR': '.',
        'PROJECT_NAME': lambda e: os.path.basename(os.path.abspath(e['PROJECT_DIR'])) + '_fpga',
        
        # Project ID to use, 8-character hex strings only.
        'PROJECT_ID': '12345678',
        
        ## Vivado Configuration
        # Path to vivado executable
        'VIVADO_PATH': '/remote/Xilinx/2020.1/Vivado/2020.1/bin/vivado',
        
        # Location to create Vivado project
        'VIVADO_PROJECT': lambda e: os.path.join(e['OUT_DIR'], 'vivado'),
        'VIVADO_PROJECT_NAME': lambda e: e['PROJECT_NAME'],

        # Top-level design entity
        'VIVADO_TOP': 'fpga_wrapper',

        # Default hw_server URL (can also use fpgagen flash --url <url>)
        'VIVADO_HW_SERVER': 'localhost:3121',
        
        # Vivado board, part, wrapper.v, and constraints.xdc
        # Automatically pulled from BOARD if unset
        # See generator/boards.py
        'VIVADO_BOARD': lambda e: BOARDS[e['BOARD']]['VIVADO_BOARD'],
        'VIVADO_PART': lambda e: BOARDS[e['BOARD']]['VIVADO_PART'],
        'FPGA_WRAPPER': lambda e: os.path.join(e['BUILTIN_DIR'], BOARDS[e['BOARD']]['FPGA_WRAPPER']),
        'FPGA_XDC': lambda e: os.path.join(e['BUILTIN_DIR'], BOARDS[e['BOARD']]['FPGA_XDC']),
    }

    def __init__(self, data:dict={}):
        self.data = {**Env.defaults, **data}

    def get(self, index: str, default=None):
        v = self.data.get(index, default)
        if callable(v): return v(self)
        else: return v
    
    def set(self, index: str, value):
        self.data[index] = value

    def merge(self, data):
        self.data = {**self.data, **data}

    def __getitem__(self, index: str):
        if index not in self.data:
            raise KeyError(f'Key {index} not found in env')
        return self.get(index)

    def __setitem__(self, index: str, value):
        return self.set(index, value)

    def __dir__(self):
        return list(self.data.keys())

    @property
    def __dict__(self):
        return {k: self.get(k) for k in self.data.keys()}
        
    def __str__(self):
        return str(self.__dict__)

    def copy(self):
        return Env(self.data.copy())
    
    def load(self, path='fpga_config.ini'):
        config = ConfigParser()
        config.read(path)
        if 'fpgagen' not in config.sections():
            return
        options = config.options('fpgagen')
        for k in options:
            self.data[k.upper()] = config.get('fpgagen', k)
    
    def save(self, path='fpga_config.ini'):
        config = ConfigParser(allow_no_value=True)
        config.optionxform = lambda x: x
        config.add_section('fpgagen')
        config.set('fpgagen', '; FPGA Environment Configuration')
        config.set('fpgagen', f'; See {self["BUILTIN_DIR"]}/generator/env.py for details')
        config.set('fpgagen', '')
        for k in self.data.keys():
            value = self.data[k]
            if k in self.defaults and value == self.defaults[k]:
                config.set('fpgagen', '; ' + k, self.get(k))
            else:
                config.set('fpgagen', k, value)

        with open(path, 'w+') as f:
            config.write(f)
    
def substitute_env(string: str, env:Env):
    return re.sub(r'\$\(([^)]+)\)', lambda v: env[v[1]], string)