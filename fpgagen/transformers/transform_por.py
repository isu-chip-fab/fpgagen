import os
import re

from ..generator.env import Env
from ..generator.transformer import Transformer, TransformerGroup, TransformingFile

"""
The POR module is ASIC magic. Replace it with a behavioral replacement
that can be driven out-of-band by the fpga_wrapper instead.

Because this change is conditional on another file existing,
rely on the fixed ordering of group transforms to check its existence in
a prior transform step
"""

def grouped_transformers():
  should_fix = False

  @Transformer('caravel/verilog/rtl/simple_por.v')
  def transform_por_file(env: Env, file: TransformingFile):
    nonlocal should_fix
    should_fix = True
    return TransformingFile(env, 'fpga/por.v', os.path.join(env['BUILTIN_DIR'], 'fpga/por.v'))

  @Transformer(re.compile(r'fpga/.*/fpga_wrapper.v'))
  def transform_fix_caravel_core(env: Env, file: TransformingFile):
    """
    Old Caravel release has no intermediate caravel_core which breaks the por code in the wrapper
    """
    nonlocal should_fix
    if should_fix:
      file.contents = file.contents.replace('caravel.chip_core.por', 'caravel.por')

  @Transformer('caravel/verilog/rtl/caravel_core.v')
  def should_fix_caravel_core(env: Env, file: TransformingFile):
    nonlocal should_fix
    should_fix = False

  return TransformerGroup(
    transform_por_file,
    should_fix_caravel_core,
    transform_fix_caravel_core
  )

transform_por = grouped_transformers()