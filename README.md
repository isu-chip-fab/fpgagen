# Running Caravel projects on an FPGA

Running Caravel projects on an FPGA is a great way to experiment with digital designs without waiting for long simulations.

This tool deals with all the nuisance of setting up and using a Vivado project from the sources in a caravel user project.

See [https://git.ece.iastate.edu/isu-chip-fab/fpgagen](https://git.ece.iastate.edu/isu-chip-fab/fpgagen) for details.

## Limitations

This sets the `FUNCTIONAL` and `SIM` macros, so functional replacements are used as in simulation. It will do a best
approximation of the final chip.

Analog and Pullup/down GPIO settings are unsupported. Modify the constraints .xdc file if those are needed.

***The generated Vivado project is considered temporary. It will be overwritten.*** Suggested usage is to only
to use the `fpgagen` command. 

If you wish to open the Vivado project directly, you may open the vivado project (see `VIVADO_PROJECT` in `fpga_config.ini`).
If you edit files in Vivado to debug, they will be overwritten by all `fpgagen` commands except `fpgagen template`.
Write a transform (see `fpgagen template`) for any changes to the caravel core you wish to persist.

This has only been tested on RHEL 8 on ISU campus. Windows compatibility is possible, but unlikely.

## Install using pip3
```sh
pip3 install fpgagen -U --user --index-url https://git.ece.iastate.edu/api/v4/projects/6467/packages/pypi/simple
```

## Simple Usage

With an Arty A7-100 FPGA plugged in on an ISU campus Linux machine, run:
```
cd caravel_user_project
fpgagen flash
```

And it will generate a Vivado project and flash the FPGA.

## Detailed Usage

By default, running `fpgagen` in a `caravel_user_project` repository will create an FPGA project, copy and modify sources
as needed, and run Vivado to generate a bitstream. `--dry-run` will create the tcl script for Vivado, but not launch it.
Useful for debugging.

Alternate options are:
```sh
fpgagen config    # Only parse and write the confguration file
fpgagen transform # Only copy and modify sources
fpgagen bitstream # Generate a bitstream (default)
fpgagen flash     # Generate bitstream and flash
fpgagen template  # Set up the transform template for adding a new transform to modify source files
                  # This is how to modify the wrapper or xdc file, etc.
```

See the configuration file `fpga_config.ini` generated when running `fpgagen` for more information on configuration options
including changing the default board and Vivado path.

## HELP! I turned off the housekeeping SPI and can't program the board!

On the Arty, hold BTN0, briefly click the RESET button, and while still holding BTN0, you will have access to the housekeeping SPI to reprogram the board.
Release BTN0 to start your program.

BTN0 is a clock disable button, while it is held, the 10MHz clock will be stopped. The RESET button is the POR reset for the board.

Holding BTN0 while resetting the board will prevent the clock from starting and the MGMT core from disabling the SPI until it is released.

## Development

To run fpgagen in development mode, from this repository run `python3 -m fpgagen -C path/to/caravel_user_project`.

To create a new release, simply update the version number in `pyproject.toml` and push to the master branch.
Run the install command above to update.