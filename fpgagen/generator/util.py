import os

def readfile(*path: str):
    with open(os.path.join(*path)) as f:
        return f.read()

def writefile(data: str, *path: str):
    p = os.path.join(*path)
    os.makedirs(os.path.dirname(p), exist_ok=True)
    with open(p, 'w') as f:
        f.write(data)