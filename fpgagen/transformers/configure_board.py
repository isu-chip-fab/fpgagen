import os

from ..generator.transformer import POSTFIX, CreateTransformer, TransformingFile
from ..generator.util import readfile

"""
Add the necessary board-support files.

For now, this is only the fpga's top-level instantiation wrapper.

Note: The top-level entity is defined as `fpga_wrapper`
"""

@CreateTransformer(POSTFIX)
def configure_board(env):
  return [
    TransformingFile(env, f'fpga/{env["BOARD"]}/{os.path.basename(env["FPGA_WRAPPER"])}', contents=readfile(env["FPGA_WRAPPER"])),
    TransformingFile(env, f'fpga/{env["BOARD"]}/{os.path.basename(env["FPGA_XDC"])}', contents=readfile(env["FPGA_XDC"])),
  ]