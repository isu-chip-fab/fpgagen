import re

from ..generator.transformer import Transformer, TransformerGroup, TransformingFile

"""
mprj_in values from the GPIO pins use an internal tristate buffer which is
ok on an ASIC, but not on an FPGA.

Note: This has been fixed in the newer Caravel wrappers, so these check if the changes really have to be done.
"""

@Transformer('caravel/verilog/rtl/gpio_control_block.v')
def fix_gpio_control_block(env, file: TransformingFile):
  """
  Remove the tristate driver in the GPIO control block.
  """
  if 'assign mgmt_gpio_in = pad_gpio_in;' in file.contents: return
  file.contents = re.sub(r'(?m)assign mgmt_gpio_in = ([^;]*);', r'assign mgmt_gpio_in = pad_gpio_in; /* \1; */', file.contents)

@Transformer('caravel/verilog/rtl/housekeeping.v')
def fix_housekeeping(env, file: TransformingFile):
  """
  Remove the lines assigning mgmt_gpio_out using tristate buffers
  """
  if 'assign mgmt_gpio_out[i] = mgmt_gpio_oeb[i] ?  1\'bz : mgmt_gpio_out_pre[i];' not in file.contents: return
  file.contents = re.sub(r'(?m)(assign mgmt_gpio_out\[[^;]*);', r'/* \1; */', file.contents)
  file.contents = re.sub(r'(?m)(/\* assign mgmt_gpio_out\[)', r'assign mgmt_gpio_out[`MPRJ_IO_PADS-1:0] = mgmt_gpio_out_pre[`MPRJ_IO_PADS-1:0];\n\1', file.contents, count=1)

@Transformer('caravel/verilog/rtl/caravel.v')
def fix_caravel(env, file: TransformingFile):
  """
  Connect mgmt_gpio_out to mgmt_io_out instead of mgmt_io_in
  """
  if 'wire [4:0] mgmt_io_out' not in file.contents: return
  file.contents = re.sub(r'(?m)wire \[4:0] mgmt_io_out', r'wire [`MPRJ_IO_PADS-1:0] mgmt_io_out', file.contents)
  file.contents = re.sub(r'(?m).mgmt_gpio_out\(mgmt_io_out\[4:2\]', r'.mgmt_gpio_out(mgmt_io_out[(`MPRJ_IO_PADS-1):(`MPRJ_IO_PADS-3)]', file.contents)
  file.contents = re.sub(r'(?m).mgmt_gpio_out\(mgmt_io_in', r'.mgmt_gpio_out(mgmt_io_out', file.contents)
  file.contents = re.sub(r'(?m).mgmt_gpio_out\({mgmt_io_out[^}]*}', r'.mgmt_gpio_out(mgmt_io_out', file.contents)

  

transform_mprj_in_tristate = TransformerGroup(
  fix_gpio_control_block,
  fix_housekeeping,
  fix_caravel,
)