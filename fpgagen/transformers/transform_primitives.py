import os
import re

from ..generator.env import Env
from ..generator.transformer import POSTFIX, CreateTransformer, Transformer, TransformerGroup, TransformingFile

"""
Most, if not all, of the primitives are broken on FPGAs.
They use user defined primitives which are not supported,
and the io blocks are almost entirely non-functional.

Replace these with custom behavioral verilog implementations
in fpga_primitives.v and fpga_io.v
"""

@Transformer([
  re.compile(r'.*sky130_(ef|fd)_io\.v'),
  re.compile(r'.*primitives\.v')
])
def remove_sky130_primitives(env: Env, file: TransformingFile):
  return False

@CreateTransformer(POSTFIX)
def add_fpga_primitives(env: Env):
  files = [
    'fpga/primitives.v',
    'fpga/io.v',
  ]
  return (TransformingFile(env, f, os.path.join(env['BUILTIN_DIR'],f)) for f in files)

transform_primitives = TransformerGroup(
  remove_sky130_primitives,
  add_fpga_primitives
)