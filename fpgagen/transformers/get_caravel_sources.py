import os
import re

from ..generator.env import Env, substitute_env
from ..generator.transformer import POSTFIX, CreateTransformer, TransformingFile
from ..generator.util import readfile

"""
Collect all the source verilog files from the user project repo
"""

def get_subproject_includes(env: Env, project_dir: str, mode: str='rtl', project=None):
  """
  Parse the verilog/includes/includes.rtl.* file to find all used verilog files
  """
  includes_dir = os.path.join(project_dir, 'verilog', 'includes')
  include_files = os.listdir(includes_dir)
  rtl_include = next(f for f in include_files if (project == None and f.startswith(f'includes.{mode}.')) or f == f'includes.{mode}.{project}')
  rtl_include_raw = readfile(includes_dir, rtl_include)
  includes = re.findall(r'^\s*-v ([^#\s]+)', rtl_include_raw, re.M)
  includes = [substitute_env(i, env) for i in includes]
  return includes  


@CreateTransformer(POSTFIX)
def get_caravel_sources(env: Env):
  # Get all verilog files included in the user project and the MCW root include files
  includes = get_subproject_includes(env, env['PROJECT_DIR'])
  includes.extend(get_subproject_includes(env, env['MCW_ROOT'], project='caravel'))
  
  # And return only those which exist
  return (TransformingFile(env, os.path.relpath(i, env['PROJECT_DIR']), i) for i in includes if os.path.exists(i))
