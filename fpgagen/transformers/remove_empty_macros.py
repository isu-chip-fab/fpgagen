import re

from ..generator.env import Env
from ..generator.transformer import Transformer, TransformingFile

"""
Caravel chip core contains some empty macros marked with (* keep *)
which look like black boxes to Vivado. Remove them.
"""

@Transformer('caravel/verilog/rtl/caravel_core.v')
def remove_empty_macros(env: Env, file: TransformingFile):
  file.contents = re.sub(r'(?m)(\(\* keep \*\).*\(\);)', r'// \1', file.contents)
  file.contents = re.sub(r'(?m)^(.*spare_logic_block[^;]+;)', r'/* \1 */', file.contents)
