import re

from ..generator.env import Env
from ..generator.transformer import Transformer, TransformingFile

"""
Set the project ID from the env
"""

@Transformer([
  'caravel/verilog/rtl/caravel.v',
  'caravel/verilog/rtl/caravel_core.v',
])
def transform_project_id(env: Env, file: TransformingFile):
  # The project ID in verilog is bit-reversed from what is entered
  binary = f"{int(env['PROJECT_ID'], base=16):032b}"
  bitreverse_projectid = f'{int(binary[::-1], base=2):08x}'
  file.contents = re.sub(r'USER_PROJECT_ID = 32\'h.{8}', f'USER_PROJECT_ID = 32\'h{bitreverse_projectid}', file.contents)
