`default_nettype wire
`default_nettype wire
module fpga_clock_divider(
    input RESETn,
    input FAST,
    output reg SLOW
);

localparam MAX = 10;     

reg [7:0] counter;

always @(posedge FAST or negedge RESETn)
begin
    counter <= counter + 1;
    if (counter == ((MAX/2)-1)) begin
        counter <= 0;
        SLOW <= ~SLOW;
    end
    if (~RESETn) begin
        counter <= 0;
        SLOW <= 0;
    end
end
endmodule
`default_nettype none



`default_nettype wire
module fpga_wrapper(
    inout gpio,
    inout [`MPRJ_IO_PADS-1:0] mprj_io,
    input fast_clk100MHz,
    input por_in,
    input reset_in,

    // Note that only two flash data pins are dedicated to the
    // management SoC wrapper.  The management SoC exports the
    // quad SPI mode status to make use of the top two mprj_io
    // pins for io2 and io3.

    inout flash_csb,
    inout flash_clk,
    inout flash_io0,
    inout flash_io1,
    inout flash_io2,
    inout flash_io3,

    output debug_ser_rx,
    output debug_ser_tx,

    output [0:15] debug
    );
    
    assign flash_io2 = 1;
    assign flash_io3 = 1;

    assign debug_ser_rx = mprj_io[5];
    assign debug_ser_tx = mprj_io[6];
    
    wire clock;
    wire resetb;
        
    assign caravel.chip_core.por.porb_h = por_in;

    reg [7:0] reset_delay;
    always @(posedge clock or negedge por_in)
        if (~por_in)
            reset_delay <= 0;
        else
            reset_delay <= {reset_delay[6:0], por_in};
    assign resetb = reset_delay[7];

    assign debug = { 4'b1111, 4'b1111, 4'b1111, 4'b1111 };
    
    fpga_clock_divider divider(
        .RESETn(por_in),
        .FAST(fast_clk100MHz),
        .SLOW(clock)
    );
    
    caravel caravel (
        .vddio(1),
        .vddio_2(1),
        .vssio(0),
        .vssio_2(0),
        .vdda(1),
        .vssa(0),
        .vccd(1),
        .vssd(0),
        .vdda1(1),
        .vdda1_2(1),
        .vdda2(1),
        .vssa1(0),
        .vssa1_2(0),
        .vssa2(0),
        .vccd1(1),
        .vccd2(1),
        .vssd1(0),
        .vssd2(0),

        .gpio(gpio),
        .mprj_io(mprj_io),
        .clock(clock & ~reset_in),
        .resetb(resetb),
        .flash_csb(flash_csb),
        .flash_clk(flash_clk),
        .flash_io0(flash_io0),
        .flash_io1(flash_io1)
    );
endmodule
`default_nettype none
`default_nettype none
