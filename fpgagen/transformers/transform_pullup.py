import re

from ..generator.transformer import Transformer, TransformingFile

"""
pullup and pulldown are unsupported in Vivado 
  and are replaced with no connection by default.

Remove references them and replace them with constant 1 and 0.
"""

@Transformer([
  re.compile(r'.*sky130_fd_sc_(hd|hvl)\.v')
])
def transform_pullup(env, file: TransformingFile):
  # Delete any line mentioning a pullup/down wire
  file.contents = re.sub(r'(?m)^\s*wire pull(up|down).*', '', file.contents)

  # Delete any line starting with pullup/down, that is an instantiation
  file.contents = re.sub(r'(?m)^\s*pull(up|down).*', '', file.contents)

  # Anything left is a usage, replace any signal starting with the name pullup/down* with a constant value
  file.contents = re.sub(r'\bpullup\w*', '1', file.contents)
  file.contents = re.sub(r'\bpulldown\w*', '0', file.contents)