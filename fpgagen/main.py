import argparse
import os, sys
from pprint import pprint
import importlib.util

from fpgagen.generator.util import readfile, writefile

def main():
  description = """\
  Caravel User Project Vivado FPGA Generation Utility

  This utility allows you to convert a caravel_user_project
  repository into a Vivado project, generate a bitstream file,
  and program the bitstream to an FPGA in one simple command.
  """

  args = argparse.ArgumentParser(prog='fpgagen', description=description, formatter_class=argparse.RawDescriptionHelpFormatter)
  args.add_argument('-c', metavar='config', dest='config', default='fpga_config.ini', help='The configuration file to read (default: fpga_config.ini)')
  args.add_argument('-C', metavar='chdir', dest='chdir', help='Change directory before doing anything')
  def common_args(argp, prefix='_'):
    argp.add_argument('-f', '--force', dest=prefix + 'force', action='store_true', help='Perform the step even if it is already up to date')
    argp.add_argument('-d', dest=prefix + 'debug', action='store_true', help='Save intermediate transform outputs for debugging')
    argp.add_argument('-D', dest=prefix + 'D', metavar='config=value', action='append', help='Temporarily override an option from the configuration file')
    argp.add_argument('--dry-run', dest=prefix + 'dry_run', action='store_true', help='For Vivado actions, create the tcl script, but do not run it')
  common_args(args, prefix='')

  subargs = args.add_subparsers(metavar='subcommand')
  commands = {}
  commands['help'] = subargs.add_parser('help', help='Get help for a subcommand')
  commands['help'].set_defaults(command='help')
  commands['help'].add_argument(dest='help', metavar='subcommand', nargs='?', choices=['config', 'transform', 'bitstream', 'flash', 'template'])

  commands['config'] = subargs.add_parser('config', help='Only parse and update the configuration file')
  commands['config'].set_defaults(command='config')

  commands['transform'] = subargs.add_parser('transform', help='Copy verilog from user project and transform as needed for Vivado')
  commands['transform'].set_defaults(command='transform')
  common_args(commands['transform'])

  commands['bitstream'] = subargs.add_parser('bitstream', help='Generate a bitstream for the configured FPGA (default)')
  commands['bitstream'].set_defaults(command='bitstream')
  common_args(commands['bitstream'])

  commands['flash'] = subargs.add_parser('flash', help='Flash the generated bitstream to a connected FPGA')
  commands['flash'].set_defaults(command='flash')
  common_args(commands['flash'])

  commands['template'] = subargs.add_parser('template', help='Create a template transformer override file')
  commands['template'].set_defaults(command='template')
  commands['template'].add_argument('-f', '--force', dest='_force', action='store_true', help='Perform the step even if it is already up to date')

  # Merge subcommand args with the default arguments
  parsed = args.parse_args()
  if hasattr(parsed, '_force') and parsed._force: parsed.force = True
  if hasattr(parsed, '_debug') and parsed._debug: parsed.debug = True
  if hasattr(parsed, '_dry_run') and parsed._dry_run: parsed.dry_run = True
  if hasattr(parsed, '_D') and parsed._D != None:
    if parsed.D != None:
      parsed.D.extend(parsed._D)
    else:
      parsed.D = parsed._D


  if not hasattr(parsed, 'command'):
    parsed.command = 'bitstream'

  if parsed.command == 'help':
    if parsed.help == None:
      args.print_help()
    else:
      commands[parsed.help].print_help()
    exit(0)


  if parsed.chdir != None:
    os.chdir(parsed.chdir)

  from .generator.env import Env
  from .generator.transformer import run_transformers
  from .generator.vivado import Vivado

  env = Env()
  env.load(parsed.config)
  env.save(parsed.config) # Update defaulted options if changed

  if parsed.command == 'config':
    exit(0)

  if parsed.command == 'template':
    path = os.path.join(env['PROJECT_DIR'], 'fpga_transformers.py')
    if os.path.exists(path) and not parsed.force:
      print('fpga_transformers.py template already exists, not overwriting without -f')
      exit(1)
    writefile(readfile(os.path.join(env['BUILTIN_DIR'], 'transformers', 'template.py')), path)
    env['TRANSFORMERS'] = path
    env.save(parsed.config)
    exit(0)

  if parsed.D != None:
    for d in parsed.D:
      (k, v) = d.split('=')
      env[k] = v

  env['DRY_RUN'] = parsed.dry_run

  if parsed.debug:
    print('\nRaw command line arguments:')
    pprint(parsed.__dict__)
    print('\nParsed configuration values: ')
    pprint(env.__dict__)
    print()

  if not os.path.exists(os.path.join(env['PROJECT_DIR'], 'caravel')):
    print(f'Not a caravel project.')
    print(f'`cd` into the project directory or set PROJECT_DIR in {parsed.config}')
    exit(1)

  # Import the transformers array dynamically so it may be overridden by the ini file
  # See templateer
  if env['TRANSFORMERS'] != env.defaults['TRANSFORMERS']:
    spec = importlib.util.spec_from_file_location("_transformers", env['TRANSFORMERS'])
    transformer_module = importlib.util.module_from_spec(spec)
    sys.modules["_transformers"] = transformer_module
    spec.loader.exec_module(transformer_module)
    transformers = transformer_module.transformers
  else:
    from .transformers import transformers


  sources = run_transformers(env, transformers, logging=parsed.debug)
  if parsed.command == 'transform': exit(0)

  vivado = Vivado(env)
  vivado.refresh_sources(env, sources, force=parsed.force)
  vivado.generate_bitstream(env, parsed.force)
  if parsed.command == 'flash':
    vivado.program_fpga(env)
  vivado.exec(env)