import re
import os
import subprocess

from .util import readfile, writefile

class Vivado:
  def __init__(self, env):
    self.tcl = []

    self.open(env)
  
  def exec(self, env):
    tcl_path = os.path.join(env['OUT_DIR'], 'tmp.tcl')
    writefile('\n'.join(self.tcl), tcl_path)
    journal = os.path.join(env['OUT_DIR'], 'vivado.jou')
    log = os.path.join(env['OUT_DIR'], 'vivado.log')
    print(f'Tcl script written to {tcl_path}')
    if not env['DRY_RUN']:
      try:
        subprocess.check_call([env['VIVADO_PATH'], '-mode', 'batch', '-journal', journal, '-log', log, '-source', tcl_path])
      except subprocess.CalledProcessError:
        print('ERROR: Vivado encountered an error.')
        print(f'ERROR: See above or the log file in {log}')
        exit(1)

  def flush_project(self):
    self.tcl.append('remove_files [get_files *]')

  def set_global_includes(self, files):
    """
    Pads and defines.v files need to be set as global include files in Vivado
    """
    SEARCH_REGEX = r'.*/(?:pads|[^/]*defines).v$'
    includes = ' '.join(f for f in files if re.match(SEARCH_REGEX, f) != None)
    self.tcl.append(f'set_property is_global_include true [get_files {{{includes}}}]')

  def add_sources(self, files):
    self.tcl.append('add_files -norecurse ' + ' '.join(files))

  def create(self, env):
    """
    Create a new project

    Also disable Vivado automatic reorganization of source files.
    They are sorted in compile order and must stay that way.
    """
    self.tcl.append(f'create_project {env["VIVADO_PROJECT_NAME"]} {env["VIVADO_PROJECT"]} -part {env["VIVADO_PART"]}')
    self.tcl.append(f'set_property board_part {env["VIVADO_BOARD"]} [current_project]')
    self.tcl.append(f'set_property source_mgmt_mode DisplayOnly [current_project]')

  def open(self, env):
    """
    Open a vivado project or create if not there
    """
    vivado_xpr = os.path.join(env['VIVADO_PROJECT'], env['VIVADO_PROJECT_NAME'] + '.xpr')
    if not os.path.exists(vivado_xpr):
      self.create(env)
    else:
      self.tcl.append(f'open_project {vivado_xpr}')

  def refresh_sources(self, env, sources, force=False):
    old_sources = os.path.join(env['VIVADO_PROJECT'], 'sources.list')
    sources_str = "\n".join(sources)
    if os.path.exists(old_sources) and not force:
      if readfile(old_sources).strip() == sources_str:
        print('Source file list up to date in Vivado Project, not refreshing')
        return
    self.flush_project()
    self.add_sources(sources)
    self.set_global_includes(sources)
    self.tcl.append(f'exec echo {{{sources_str}}} > {{{old_sources}}}')

  def find_bitstream(self, env):
    return os.path.join(env['VIVADO_PROJECT'], env['VIVADO_PROJECT_NAME'] + '.runs', 'impl_1', 'fpga_wrapper.bit')

  def generate_bitstream(self, env, force=False):
    self.tcl.append(f'set_property top {env["VIVADO_TOP"]} [current_fileset]')
    self.tcl.append('set_property STEPS.PHYS_OPT_DESIGN.IS_ENABLED true [get_runs impl_1]')
    if force:
      self.tcl.append("""\
reset_run synth_1
launch_runs synth_1
wait_on_run synth_1
reset_run impl_1
launch_runs impl_1 -to_step write_bitstream
wait_on_run impl_1
""")
    else:
      self.tcl.append("""\
if {[get_property PROGRESS [get_runs synth_1]] != "100%" || [get_property NEEDS_REFRESH [get_runs synth_1]]} {
  reset_run synth_1
  launch_runs synth_1
  wait_on_run synth_1
  if {[get_property PROGRESS [get_runs synth_1]] != "100%"} {
    exit
  }
}
if {[get_property PROGRESS [get_runs impl_1]] != "100%" || [get_property NEEDS_REFRESH [get_runs impl_1]]} {
  reset_run impl_1
  launch_runs impl_1 -to_step write_bitstream
  wait_on_run impl_1
  if {[get_property PROGRESS [get_runs impl_1]] != "100%"} {
    exit
  }
}
""")
    return self.find_bitstream(env)

  def program_fpga(self, env, bitstream=None):
    if bitstream == None:
      bitstream = self.find_bitstream(env)
    
    self.tcl.append(f"""\
open_hw_manager
connect_hw_server -url {env["VIVADO_HW_SERVER"]}
open_hw_target
set_property PROGRAM.FILE {{{bitstream}}} [current_hw_device]
program_hw_device
""")